﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using TheWeather.Models;
using TheWeather.Services;
using Xamarin.Forms;

namespace TheWeather.ViewModels
{
    public class weatherPageViewModel : INotifyPropertyChanged
    {
        //Cuando agregamos INotifyPropertyChanged debemos agregar la siguientes linea de codigo
        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion


        private WeatherData data;
        public WeatherData Data
        {
            get => data;
            set
            {
                data = value;
                OnPropertyChanged();
            }
        }

        public Command SearchCommand { get; set; }

        public weatherPageViewModel()
        {
            SearchCommand = new Command(async (searchTerm) =>
            {
                //pasar valores del SearchBar a nuestro ws
                //x,y
                var entrada = searchTerm as string;
                var datos = entrada.Split(',');
                var lat = datos[0];
                var lon = datos[1];
                Data = await new Api<WeatherData>().Get($"https://api.weatherbit.io/v2.0/current?lat={lat}&lon={lon}&key=b00bb81a71b146a19a1154ab883ded13&lang=es");

            });
        }

    }
}

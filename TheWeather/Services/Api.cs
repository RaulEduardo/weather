﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheWeather.Services
{
    public class Api<T>
    {
        public async Task<T> Get(string Url)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(Url);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var responseString = await response.Content.ReadAsStringAsync();

                        return JsonConvert.DeserializeObject<T>(responseString);
                    }
                    return default;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<T>> GetAll(string Url)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(Url);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var responseString = await response.Content.ReadAsStringAsync();

                        return JsonConvert.DeserializeObject<List<T>>(responseString);
                    }
                    return default;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<T> PostData(T contenido)
        {
            using (var httpclient = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(contenido);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await httpclient.PostAsync("", content);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
                }
                else
                    throw new Exception("No se guardo");

            }
        }

        /*public async Task<ErrorResponse> PostDataSave(T contenido)
        {
            using (var httpclient = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(contenido);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await httpclient.PostAsync("", content);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return JsonConvert.DeserializeObject<ErrorResponse>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    throw new Exception("No se guardo");
                }
            }
        }*/

    }
}

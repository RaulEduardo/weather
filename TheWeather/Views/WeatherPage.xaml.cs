﻿using System;
using System.Collections.Generic;
using TheWeather.ViewModels;
using Xamarin.Forms;

namespace TheWeather.Views
{
    public partial class WeatherPage : ContentPage
    {
        public WeatherPage()
        {
            InitializeComponent();
            BindingContext = new weatherPageViewModel();
        }
    }
}
